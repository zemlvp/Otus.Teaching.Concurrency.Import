﻿using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using System.IO;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Otus.Teaching.Concurrency.Import.Loader.Parsers
{
    public class CustomersDataParser : IDataParser<CustomersList>
    {
        private readonly string _fileName;

        public CustomersDataParser(string fileName)
        {
            _fileName = fileName;
        }

        public CustomersList Parse()
        {
            CustomersList customers = null;

            using (var stream = File.OpenRead(_fileName))
            {
                customers = new XmlSerializer(typeof(CustomersList)).Deserialize(stream) as CustomersList;
            }

            return customers;
        }

        public async Task<CustomersList> ParseAsync()
        {
            return await Task.Run(() => Parse());
        }
    }

}
