﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Loader.Loaders;
using Otus.Teaching.Concurrency.Import.Loader.Parsers;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    class Program
    {
        private static readonly string _dataFileDirectory = AppDomain.CurrentDomain.BaseDirectory;
        private static string _dataFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "customers.xml");
        private static string _dataGeneratorAppExe;
        private static readonly int _threadsCount = 30;
        private static readonly int _countGenerateCustomers = 100_000;

        static void Main(string[] args)
        {
            // Если указан один параметр, то первый параметр путь к customers.xml
            /*
              customers
             */
            // Если указано два параметра, то
            // первый параметр - путь к Otus.Teaching.Concurrency.Import.DataGenerator.App.exe
            // второй параметр - путь к customers.xml
            // args: D:\OTUS\ДЗ\18\Otus.Teaching.Concurrency.Import\Otus.Teaching.Concurrency.Import.DataGenerator.App\bin\Debug\netcoreapp3.1\Otus.Teaching.Concurrency.Import.DataGenerator.App.exe customers

            HandleArgs(args);

            Console.WriteLine($"Main process Id {Process.GetCurrentProcess().Id}...");

            if (_dataGeneratorAppExe != null)
                GenerateCustomersFileFromProcess();
            else
                GenerateCustomersDataFile();

            var parser = new CustomersDataParser(_dataFilePath);
            
            Task<CustomersList> parseTask = parser.ParseAsync();

            var customers = parseTask.Result;
            
            Console.WriteLine("xml файл с {0} записями", customers.Customers.Count);
            Console.WriteLine();

            //SqlLite(customers);
            LocalDb(customers);

            Console.ReadKey();
        }

        private static void SqlLite(CustomersList customers)
        {
            var customerDataLoaderSqlLite = new CustomerDataLoaderSqlLite(customers);
            customerDataLoaderSqlLite.GenerateDatabase();

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            // в одном потоке загрузка в базу данных
            //customerDataLoaderSqlLite.LoadData();

            //многопоточная загрузка в базу данных
            customerDataLoaderSqlLite.LoadDataInThreads(_threadsCount);

            stopWatch.Stop();
            TimeSpan ts = stopWatch.Elapsed;

            string elapsedTime = string.Format("{0}мин {1}сек {2}мс", ts.Minutes, ts.Seconds, ts.Milliseconds);

            Console.WriteLine("Готово {0}!!! ", elapsedTime);
        }

        private static void LocalDb(CustomersList customers)
        {
            var customerDataLoaderLocalDb = new CustomerDataLoaderLocalDb(customers);
            customerDataLoaderLocalDb.GenerateDatabase();

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            // в одном потоке загрузка в базу данных
            //customerDataLoaderLocalDb.LoadData();

            //многопоточная загрузка в базу данных
            customerDataLoaderLocalDb.LoadDataInThreads(_threadsCount);

            stopWatch.Stop();
            TimeSpan ts = stopWatch.Elapsed;

            string elapsedTime = string.Format("{0}мин {1}сек {2}мс", ts.Minutes, ts.Seconds, ts.Milliseconds);

            Console.WriteLine("Готово {0}!!! ", elapsedTime);
        }

        private static void HandleArgs(string[] args)
        {
            if (args == null || !args.Any())
                throw new ArgumentException("Required command line arguments");

            if (args.Length == 2)
            {
                _dataGeneratorAppExe = args[0];
                _dataFilePath = Path.Combine(_dataFileDirectory, $"{args[1]}.xml");

                Console.WriteLine("_dataGeneratorAppExe={0}", _dataGeneratorAppExe);
                Console.WriteLine("_dataFilePath={0}", _dataFilePath);
            }
            else if (args.Length == 1)
            {
                _dataFilePath = Path.Combine(_dataFileDirectory, $"{args[0]}.xml"); ;
                Console.WriteLine("_dataFilePath={0}", _dataFilePath + ".xml");
            }
            else
            {
                throw new ArgumentException("Required right command line arguments");
            }
        }

        private static void GenerateCustomersFileFromProcess()
        {
            Console.WriteLine("Генерация файла из процесса");

            var proc = new Process()
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = _dataGeneratorAppExe,
                    Arguments = _dataFilePath
                }
            };
            proc.Start();
            Console.WriteLine($"Loader started with process Id {proc.Id}...");
        }

        private static void GenerateCustomersDataFile()
        {
            Console.WriteLine("Генерация файла из метода...");

            Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");

            var xmlGenerator = new XmlGenerator(_dataFilePath, _countGenerateCustomers);
            xmlGenerator.Generate();

            Console.WriteLine("Окончание генерации файла из метода...");
        }
    }
}