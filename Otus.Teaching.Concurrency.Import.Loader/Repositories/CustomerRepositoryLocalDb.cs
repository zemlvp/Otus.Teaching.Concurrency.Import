﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.DataAccess.LocalDb;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Loader.Repositories
{
    public class CustomerRepositoryLocalDb : ICustomerRepository
    {
        private readonly DataContextLocalDb _dataContext;

        public CustomerRepositoryLocalDb(DataContextLocalDb datacontext)
        {
            _dataContext = datacontext;
        }
        public void AddCustomer(Customer customer)
        {
            _dataContext.Customers.Add(new Customer
            {
                FullName = customer.FullName,
                Email = customer.Email,
                Phone = customer.Phone
            });
        }
        public async Task<Customer> AddCustomerAsync(Customer customer)
        {
            var entry = await _dataContext.Customers.AddAsync(new Customer
            {
                FullName = customer.FullName,
                Email = customer.Email,
                Phone = customer.Phone
            });

            await _dataContext.SaveChangesAsync();

            return entry.Entity;
        }

        public async Task<Customer> GetCustomerAsync(int id)
        {
            return await _dataContext.Customers.FindAsync(id);            
        }

        public async Task<Customer> GetCustomerByNameAsync(string fullName)
        {
            return await _dataContext.Customers.FirstOrDefaultAsync(a => a.FullName == fullName);
        }
    }
}
