﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataAccess.LocalDb;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Loader.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    public class CustomerDataLoaderLocalDb : IDataLoader
    {
        private CustomersList _allCustomers;

        public CustomerDataLoaderLocalDb(CustomersList customers)
        {
            Console.WriteLine("LocalDb:");
            _allCustomers = customers;
        }

        public void GenerateDatabase()
        {
            using var dataContext = new DataContextLocalDb();
            new DbLocalDbInitializer(dataContext).InitializeDatabase();
        }

        public void LoadData()
        {
            Console.WriteLine("В одном потоке загрузка в базу данных {0} записей", _allCustomers.Customers.Count);
            LoadToDatabase(_allCustomers.Customers);
        }

        private void LoadToDatabase(IEnumerable<Customer> customers)
        {
            Console.WriteLine("Loading data ThreadId: {0}...", Thread.CurrentThread.ManagedThreadId);

            using (var dataContext = new DataContextLocalDb())
            {
                var i = 0;
                var customerRepositorySqlLite = new CustomerRepositoryLocalDb(dataContext);
                foreach (var customer in customers)
                {
                    var task = customerRepositorySqlLite.AddCustomerAsync(customer);
                    
                    i++;
                    if (i % 10000 == 0)
                    {
                        Console.WriteLine("ThreadId {0}: i={1}", Thread.CurrentThread.ManagedThreadId, i);
                        SaveChanges(dataContext);
                    }
                }
                
                SaveChanges(dataContext);
            }

            Console.WriteLine("Loaded data ThreadId: {0}... Количество записей={1}", Thread.CurrentThread.ManagedThreadId, customers.Count());
        }

        private void SaveChanges(DataContextLocalDb dataContext)
        {
            try
            {
                dataContext.SaveChanges();
            }
            catch (DbUpdateException ex)
            {   
                Console.WriteLine("Блокировка - прерываем поток {0} на 10с: {1}", Thread.CurrentThread.ManagedThreadId, ex.InnerException != null ? ex.InnerException.Message : ex.Message);
                Thread.Sleep(10000);
                SaveChanges(dataContext);
            }
        }

        public void LoadDataInThreads(int threadsCount)
        {
            var allCustomersCount = _allCustomers.Customers.Count();
            Console.WriteLine("В {0} потоках загрузка в базу данных {1} записей", threadsCount, allCustomersCount);

            WaitHandle[] waitHandles = new WaitHandle[threadsCount];

            var pageSize = allCustomersCount / threadsCount + 1;

            for (int i = 1; i <= threadsCount; i++)
            {
                var customersDiaposon = _allCustomers.Customers.Skip((i - 1) * pageSize).Take(pageSize);

                var handle = new EventWaitHandle(false, EventResetMode.ManualReset);
                var thread = new Thread(() =>
                {
                    LoadToDatabase(customersDiaposon);
                    handle.Set();
                });
                waitHandles[i - 1] = handle;
                thread.Start();
            }
            _allCustomers = null;
            WaitHandle.WaitAll(waitHandles);
        }
    }
}
