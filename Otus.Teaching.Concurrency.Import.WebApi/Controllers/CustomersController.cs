﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Concurrency.Import.DataAccess.LocalDb;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using Otus.Teaching.Concurrency.Import.Loader.Repositories;

namespace Otus.Teaching.Concurrency.Import.WebApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        private ICustomerRepository _customerRepositoryLocalDb;
        private DataContextLocalDb _dataContext;

        public CustomersController()
        {
            _dataContext = new DataContextLocalDb();
            _customerRepositoryLocalDb = new CustomerRepositoryLocalDb(_dataContext);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Customer>> GetCustomerAsync(int id)
        {
            var customer = await _customerRepositoryLocalDb.GetCustomerAsync(id);
            if (customer == null)
                return NotFound();  

            return Ok(customer);  
        }

        [HttpPost]
        public async Task<ActionResult<Customer>> AddCustomerAsync([FromBody] Customer customerDto)
        {
            var customer = await _customerRepositoryLocalDb.GetCustomerByNameAsync(customerDto.FullName);

            if (customer != null)
                return StatusCode(409, "Пользователь уже существует!");
                
            var newCustomer = await _customerRepositoryLocalDb.AddCustomerAsync(customerDto);  

            return Ok(newCustomer); // 200
        }
    }
}
