﻿using Newtonsoft.Json;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.TestWebApiConsoleApp
{
    class Program
    {
        private static readonly string WEB_API = "http://localhost:52258";
        private static readonly HttpClient _client = new HttpClient { BaseAddress = new Uri(WEB_API) };

        static void Main()
        {
            GetCustomer().ConfigureAwait(false).GetAwaiter().GetResult();
            AddCustomer().ConfigureAwait(false).GetAwaiter().GetResult();
            Console.ReadKey();
        }

        private static async Task GetCustomer()
        {
            Console.WriteLine("Введите Id кастомера для получения через web api:");
            var id = int.Parse(Console.ReadLine());

            var customer = await GetCustomerAsync(id);

            if (customer != null)
                Console.WriteLine($"Получен кастомер Id={id} через web api: FullName={customer.FullName}");
            else
                Console.WriteLine($"Кастомер с Id={id} не найден!");
        }

        private static async Task AddCustomer()
        {
            var newCustomer = new Customer
            {
                FullName = "test" + new Random().Next(int.MaxValue - 1000, int.MaxValue)
                //FullName = "test2147482970" //тест если уже есть кастомер
            };

            var c = await AddCustomerAsync(_client, newCustomer);
            if (c != null)
                Console.WriteLine($"Добавлен кастомер через web api: FullName={c.FullName}, Id={c.Id}");
            else
                Console.WriteLine($"Кастомер с FullName={newCustomer.FullName} уже существует!");
        }

        async static Task<Customer> GetCustomerAsync(int id)
        {
            var response = await _client.GetAsync($"customers/{id}");

            if (response.StatusCode == HttpStatusCode.NotFound)
                return null;

            var result = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<Customer>(result);
        }

        async static Task<Customer> AddCustomerAsync(HttpClient client, Customer customer)
        {
            var content = new StringContent(JsonConvert.SerializeObject(customer), System.Text.Encoding.UTF8, "application/json");
            var response = await client.PostAsync("customers", content);
            
            if (response.StatusCode == HttpStatusCode.Conflict)
                return null;

            var result = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<Customer>(result);
        }
    }
}