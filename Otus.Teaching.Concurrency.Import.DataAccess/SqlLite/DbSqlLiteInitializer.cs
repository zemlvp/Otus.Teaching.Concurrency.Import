﻿namespace Otus.Teaching.Concurrency.Import.DataAccess.SqlLite
{
    public class DbSqlLiteInitializer
    {
        private readonly DataContextSqlLite _dataContext;

        public DbSqlLiteInitializer(DataContextSqlLite dataContext)
        {
            _dataContext = dataContext;
        }

        public void InitializeDatabase()
        {
            System.Console.WriteLine("Создание БД...");
            _dataContext.Database.EnsureDeleted();
            _dataContext.Database.EnsureCreated();
        }
    }
}
