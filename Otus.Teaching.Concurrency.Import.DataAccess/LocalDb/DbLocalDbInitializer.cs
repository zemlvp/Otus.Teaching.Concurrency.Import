﻿namespace Otus.Teaching.Concurrency.Import.DataAccess.LocalDb
{
    public class DbLocalDbInitializer
    {
        private readonly DataContextLocalDb _dataContext;

        public DbLocalDbInitializer(DataContextLocalDb dataContext)
        {
            _dataContext = dataContext;
        }

        public void InitializeDatabase()
        {
            System.Console.WriteLine("Создание БД...");
            _dataContext.Database.EnsureDeleted();
            _dataContext.Database.EnsureCreated();
        }
    }
}
