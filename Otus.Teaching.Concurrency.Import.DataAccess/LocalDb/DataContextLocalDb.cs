﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;

namespace Otus.Teaching.Concurrency.Import.DataAccess.LocalDb
{
    public class DataContextLocalDb : DbContext
    {
        public DbSet<Customer> Customers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=Customers;Trusted_Connection=True;MultipleActiveResultSets=true;", 
                builder => {
                builder.EnableRetryOnFailure(10, TimeSpan.FromSeconds(10), null);
            });
        }
    }
}
